package br.com.galgo.testes.suite;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.testes.recursos_comuns.categoria.CategoriaDownload;
import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSistema;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.teste.TesteConsultaPLCota;
import br.com.galgo.testes.recursos_comuns.teste.TesteConsultaFundos;
import br.com.galgo.testes.recursos_comuns.utils.SuiteUtils;

@RunWith(Categories.class)
@IncludeCategory(CategoriaDownload.class)
@Suite.SuiteClasses({ TesteConsultaPLCota.class,//
		TesteConsultaFundos.class //
})
public class SuiteDownload {

	private static final String PASTA_SUITE = "Download";

	@BeforeClass
	public static void setUp() throws Exception {
		SuiteUtils.configurarSuiteDefault(Ambiente.PRODUCAO, PASTA_SUITE);

		final String caminhoEvidenciaTarget = ConfiguracaoSistema
				.getCaminhoEvidenciaTarget("");

		System.out.println("Criando diretorio de evidencia: "
				+ caminhoEvidenciaTarget);

		FileUtils.forceMkdir(new File(caminhoEvidenciaTarget));
	}
}
